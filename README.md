### McMenuLib
This is a library for creating Minecraft menus easier.<br>
Available types:<br>
1. StandardMenu
2. PaginatedMenu


##### StandardMenu
Can be used as global menu or as "per player" menu.
It has a single page and can be refreshed using #refresh method.
You can handle click and refresh events for each menu elements.
You need to do `MenuLib#init(itemStackSupport, plugin)` at #onEnable.

##### PaginatedMenu

### Maven

```
<repository>
  <id>gitlab-maven</id>
  <url>https://gitlab.com/api/v4/projects/16009776/packages/maven</url>
</repository>

```

```
<dependency>
  <groupId>com.andrei1058</groupId>
  <artifactId>McMenuLib</artifactId>
  <version>1.0-SNAPSHOT</version>
</dependency>
```


#### Required
```
<repository>
  <id>gitlab-maven</id>
  <url>https://gitlab.com/api/v4/projects/14877570/packages/maven</url>
</repository>

```

```
<dependency>
  <groupId>com.andrei1058.mcversionsupport</groupId>
  <artifactId>itemstack-version</artifactId>
  <version>1.1</version>
</dependency>
```