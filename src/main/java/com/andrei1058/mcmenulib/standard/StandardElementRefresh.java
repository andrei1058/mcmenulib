package com.andrei1058.mcmenulib.standard;

public interface StandardElementRefresh {

    /**
     * Manage your element on refresh.
     */
    void onRefresh(StandardMenuElement sme);
}
