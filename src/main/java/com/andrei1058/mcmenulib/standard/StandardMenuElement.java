package com.andrei1058.mcmenulib.standard;

import com.andrei1058.mcmenulib.MenuLib;
import org.bukkit.inventory.ItemStack;

public class StandardMenuElement {

    private ItemStack itemStack;
    private StandardElementClick onClick = null;
    private StandardElementRefresh onRefresh = null;
    private String identifier = null;

    public StandardMenuElement(String identifier, ItemStack itemStack) {
        this.identifier = identifier;
        if (identifier == null || identifier.isEmpty()) {
            this.itemStack = itemStack;
        } else {
            this.itemStack = MenuLib.getInstance().getItemStackSupport().addTag(itemStack, "StandardMenuElement", identifier);
        }
    }
    public StandardMenuElement(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public void setItemStack(ItemStack itemStack) {
        if (identifier == null || identifier.isEmpty()) {
            this.itemStack = itemStack;
        } else {
            this.itemStack = MenuLib.getInstance().getItemStackSupport().addTag(itemStack, "StandardMenuElement", identifier);
        }
    }

    public StandardElementClick getOnClick() {
        return onClick;
    }

    public StandardMenuElement setOnClick(StandardElementClick onClick) {
        this.onClick = onClick;
        return this;
    }

    public StandardElementRefresh getOnRefresh() {
        return onRefresh;
    }

    public StandardMenuElement setOnRefresh(StandardElementRefresh onRefresh) {
        this.onRefresh = onRefresh;
        return this;
    }

    protected String getIdentifier() {
        return identifier;
    }
}
