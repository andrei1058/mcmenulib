package com.andrei1058.mcmenulib.standard;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

public interface StandardElementClick {

    /**
     * Manage click action.
     */
    void onClick(ClickType clickType, Player player);
}
