package com.andrei1058.mcmenulib.standard;

import com.andrei1058.mcmenulib.MenuLib;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class StandardMenuListener implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent e){
        if (e == null) return;
        if (e.getClickedInventory() == null) return;
        if (e.getWhoClicked().getOpenInventory().getTopInventory().getHolder() instanceof StandardMenu.StandardMenuHolder){
            e.setCancelled(true);
        }
        if (e.getClickedInventory().getHolder() instanceof StandardMenu.StandardMenuHolder){
            e.setCancelled(true);

            if (e.getCurrentItem() == null) return;
            if (e.getCurrentItem().getType() == Material.AIR) return;
            if (!(e.getWhoClicked() instanceof Player)) return;
            StandardMenu sm = ((StandardMenu.StandardMenuHolder)e.getClickedInventory().getHolder()).getStandardMenu();
            if (sm == null) return;
            if (!MenuLib.getInstance().getItemStackSupport().hasTag(e.getCurrentItem(), "StandardMenuElement")) return;
            StandardMenuElement sme = sm.getElement(MenuLib.getInstance().getItemStackSupport().getTag(e.getCurrentItem(), "StandardMenuElement"));
            if (sme == null) return;
            if (sme.getOnClick() == null) return;
            sme.getOnClick().onClick(e.getClick(), (Player) e.getWhoClicked());
        }
    }
    //todo manage moving
}
