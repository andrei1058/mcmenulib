package com.andrei1058.mcmenulib.standard;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class StandardMenu {

    private String identifier;
    private Inventory inv;
    private LinkedList<StandardMenuElement> unFixedMenuElements = new LinkedList<>();
    private ConcurrentHashMap<StandardMenuElement, Integer> fixedMenuElements = new ConcurrentHashMap<>();

    /**
     * This will create a chest type menu.
     *
     * @param identifier - is a string that identifies the menu. Please use a unique identifier.
     */
    public StandardMenu(String identifier, String invTitle, int invRows) {
        this.identifier = identifier;
        if (invRows > 6) invRows = 6;
        StandardMenuHolder smh = new StandardMenuHolder();
        smh.setStandardMenu(this);
        inv = Bukkit.createInventory(smh, invRows * 9, ChatColor.translateAlternateColorCodes('&', invTitle));
    }

    public void open(Player player){
        if (player == null) return;
        player.closeInventory();
        player.openInventory(inv);
    }

    /**
     * This will refresh the inventory content.
     */
    public void refresh() {
        for (Map.Entry<StandardMenuElement, Integer> e : fixedMenuElements.entrySet()) {
            if (e.getKey().getOnRefresh() != null) e.getKey().getOnRefresh().onRefresh(e.getKey());
            inv.setItem(e.getValue(), e.getKey().getItemStack());
        }
        for (int slot = 0; slot < inv.getSize(); slot++){
            if (!fixedMenuElements.containsValue(slot)) inv.setItem(slot, null);
        }
        for (StandardMenuElement sme : unFixedMenuElements) {
            if (sme.getOnRefresh() != null) sme.getOnRefresh().onRefresh(sme);
            inv.addItem(sme.getItemStack());
        }
        /*for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.getOpenInventory().getTopInventory().getHolder() instanceof StandardMenuHolder) {
                if (((StandardMenuHolder) p.getOpenInventory().getTopInventory().getHolder()).getStandardMenu().equals(this)) {
                    p.updateInventory();
                }
            }
        }*/
    }

    /**
     * Add an element to the menu.
     */
    public StandardMenu addElement(StandardMenuElement standardMenuElement) {
        this.unFixedMenuElements.add(standardMenuElement);
        inv.addItem(standardMenuElement.getItemStack());
        return this;
    }

    /**
     * Add an element to a target slot.
     */
    public StandardMenu addElement(int slot, StandardMenuElement standardMenuElement) {
        this.fixedMenuElements.put(standardMenuElement, slot);
        inv.setItem(slot, standardMenuElement.getItemStack());
        return this;
    }

    protected StandardMenuElement getElement(String identifier){
        if (identifier == null) return null;
        for (StandardMenuElement sme : unFixedMenuElements){
            if (sme.getIdentifier() == null) continue;
            if (sme.getIdentifier().equals(identifier)) return sme;
        }

        for (StandardMenuElement sme : fixedMenuElements.keySet()){
            if (sme.getIdentifier() == null) continue;
            if (sme.getIdentifier().equals(identifier)) return sme;
        }
        return null;
    }

    protected class StandardMenuHolder implements InventoryHolder {
        private StandardMenu standardMenu = null;

        public Inventory getInventory() {
            return inv;
        }

        protected void setStandardMenu(StandardMenu standardMenu) {
            this.standardMenu = standardMenu;
        }

        public StandardMenu getStandardMenu() {
            return standardMenu;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof StandardMenu)) return false;
        return ((StandardMenu) o).identifier.equalsIgnoreCase(this.identifier);
    }
}
