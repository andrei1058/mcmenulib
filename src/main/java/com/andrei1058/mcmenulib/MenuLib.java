package com.andrei1058.mcmenulib;

import com.andrei1058.mcmenulib.standard.StandardMenuListener;
import com.andrei1058.spigot.versionsupport.ItemStackSupport;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public class MenuLib {

    private ItemStackSupport itemStackSupport;
    private Plugin plugin;

    private static MenuLib instance = null;

    private MenuLib(ItemStackSupport itemStackSupport, Plugin plugin){
        this.itemStackSupport = itemStackSupport;
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(new StandardMenuListener(), getPlugin());
        instance = this;
    }

    public static MenuLib getInstance() {
        return instance;
    }

    public Plugin getPlugin() {
        return plugin;
    }

    public ItemStackSupport getItemStackSupport() {
        return itemStackSupport;
    }

    public static void init(ItemStackSupport itemStackSupport, Plugin plugin){
        if (instance != null) return;
        new MenuLib(itemStackSupport, plugin);
    }
}
